# TidyTuesday

Arquivo com as imagens criadas durante os eventos do TidyTuesday.

## Sobre

TidyTuesday é promovido pelo R4DS Online Learning Community.
Maiores detalhes em https://github.com/rfordatascience/tidytuesday.

## Primeiros Passos

1. Abra o arquivo [seu-nome.R](yyyy-mm-dd/seu-nome.R) no RStudio.
2. Salve uma cópia do arquivo onde o nome do arquivo deve ser o seu nome.
3. Procure por 'TODO' e siga as diretrizes.

## Compartilhe sua Imagem

Envie o arquivo R que criou para [incoming+gurstatsop-tidytuesday-16914042-issue-@incoming.gitlab.com](incoming+gurstatsop-tidytuesday-16914042-issue-@incoming.gitlab.com).